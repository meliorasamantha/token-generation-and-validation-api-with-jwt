[HttpGet]
[Route("GenerateToken")]
public ActionResult GenerateToken()
{
    // Generate a random secret key (replace with your own secret key)
    string secretKey = "Koding123";

    // Create the security key from the secret key
    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

    // Create the signing credentials using the security key
    var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

    // Create claims for the token (you can customize this according to your needs)
    var claims = new[]
    {
        new Claim(ClaimTypes.Name, "samantha"),
        new Claim(ClaimTypes.Email, "samantha@example.com")
    };

    // Create the token descriptor
    var tokenDescriptor = new SecurityTokenDescriptor
    {
        Subject = new ClaimsIdentity(claims),
        Expires = DateTime.UtcNow.AddMinutes(30), // Set the token expiration time
        SigningCredentials = signingCredentials
    };

    // Create the token handler
    var tokenHandler = new JwtSecurityTokenHandler();

    // Generate the token
    var token = tokenHandler.CreateToken(tokenDescriptor);

    // Write the token as a string
    var jwtToken = tokenHandler.WriteToken(token);

    Console.WriteLine($"Generated JWT Token: {jwtToken}");

    // Set the response Content-Type header explicitly
    Response.Headers.Add("Content-Type", "application/json");

    if (string.IsNullOrEmpty(jwtToken))
    {
        return BadRequest("Failed to generate token.");
    }

    return Ok(jwtToken);
}
